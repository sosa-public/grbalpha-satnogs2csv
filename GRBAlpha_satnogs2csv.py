import pandas as pd
import numpy as np
import csv
import sys
import base64
import struct
from datetime import datetime

# get filename or exit
if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    exit()

comDataHeader = [
    "uptime", "uptime_since_last_reset", "rstCnt", "McuVolt", "AuxVolt",
    "CPU_core_temperature", "PaNtcRaw", "sigRxImmediate", "sigRxAvg",
    "sigRxMax", "sigBackgroundImmediate", "sigBackgroundAvg",
    "sigBackgroundMax", "RX_packets_received", "RX_packets_transmitted",
    "AX_packets_received", "AX_packets_transmitted", "digipeaterRxCount",
    "digipeaterTxCount", "RF_CSP_packets_received",
    "RF_CSP_packets_transmitted", "I2C1_packets_received",
    "I2C1_packets_transmitted", "I2C2_packets_received",
    "I2C2_packets_transmitted", "RS485_packets_received",
    "RS485_packets_transmitted", "IF_CSP_packets_received",
    "IF_CSP_packets_transmitted", "crc", "satnogs_time"
]

obcDataHeader = [
    "timestamp", "MCU_temp", "tmp112_X+", "tmp112_Y+", "tmp112_X-",
    "tmp112_Y-", "tmp112_Z-", "magMMC_X", "magMMC_Y", "magMMC_Z", "magMPU_X",
    "magMPU_Y", "magMPU_Z", "MPU_T", "gyrMPU_X", "gyrMPU_Y", "gyrMPU_Z",
    "accMPU_X", "accMPU_Y", "accMPU_Z", "uptimeRst", "uptimeTot", "resetCnt",
    "packetRscCnt", "sstempY-", "sstempY+", "sstempX+", "sstempX-", "sstempZ-",
    "ssiradY-", "ssiradY+", "ssiradX+", "ssiradX-", "ssiradZ-", "gpsRstCnt",
    "gpsFixQuality", "gpsTracked", "gpsTemp", "freeMem", "crc", "satnogs_time"
]

df_tmp = pd.read_csv(filename, delimiter='|', header=None)

fff = open("GRBAlpha_satnogs_COMd_processed.csv", 'w', newline='')
comd_writer = csv.writer(fff, delimiter=';')
comd_writer.writerow(comDataHeader)

ffff = open("GRBAlpha_satnogs_OBC_processed.csv", 'w', newline='')
obc_writer = csv.writer(ffff, delimiter=';')
obc_writer.writerow(obcDataHeader)

for index, row in df_tmp.iterrows():
    byte_arr = bytes.fromhex(row[1])
    try:
        pay_str = byte_arr[16:].decode()
    except:
        continue

    comd_i = pay_str.find(",COMd,")
    if comd_i == -1:
        # base64 OBC
        if (len(pay_str) % 4) != 0:
            try:
                decoded = base64.b64decode(pay_str[:-1])
            except:
                continue

            if (len(decoded) == 92):
                obcDataModel = list(
                    struct.unpack('LhhhhhhhhhhhhfhhhhhhLLLLHHHHHHHHHHLBBhHH',
                                  decoded))
                obcDataModel.append(
                    datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S"))
                obc_writer.writerow(obcDataModel)

    else:
        # sounds like COMd packet
        parts = pay_str.split(',')
        ii = 2
        ei = 0
        if (len(parts) == 45 and parts[1] == "COMd"):
            if (parts[ii] == "U"):
                comDataModel = np.empty_like(comDataHeader)
                comDataModel[:] = np.nan
                ii += 1  # 3
                ei = 0
                comDataModel[ei] = float(parts[ii])  #uptime
                ei += 1
                comDataModel[ei] = float(parts[ii + 1])  #uptime since reset
                ei += 1
                ii += 3
                comDataModel[ei] = float(parts[ii])  # reset cnt
                ei += 1
                ii += 2
                comDataModel[ei] = float(parts[ii])  # mcu volt
                ei += 1
                ii += 2  # 10
                comDataModel[ei] = float(parts[ii])
                ei += 1
                ii += 2
                comDataModel[ei] = float(parts[ii])  # temp core
                ei += 1
                comDataModel[ei] = float(parts[ii + 1])  # temp ntc
                ei += 1
                ii += 3  #15
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1  # 20
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 25
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 28
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 31
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 34
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1  # 35
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 40
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 2  # 43
                comDataModel[ei] = float(parts[ii])  #
                ei += 1
                ii += 1
                comDataModel[ei] = float(parts[ii])
                ei += 1
                comDataModel[ei] = 0  # crc?
                ei += 1
                comDataModel[ei] = datetime.strptime(row[0],
                                                     "%Y-%m-%d %H:%M:%S")

                comd_writer.writerow(comDataModel)

fff.close()
ffff.close()