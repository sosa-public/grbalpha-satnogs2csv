# Quick and dirty converter of Satnogs GRBAlpha packets to CSV
Pass a csv file donwloaded from satnogs network as an argument.

Example: ``$ python GRBAlpha_satnogs2csv.py 99722-1675-20210327T100540Z-all.csv``

The script will decode the csv downloaded from satnogs to a semicolon (;) delimited CSV for OBC and COM packets of GRBAlpha satellite. Two files are produced (overwritten if already present), one with OBC log and one with COM log.

**No processing is done on the unpacked data!**

Data is downloadable from https://db.satnogs.org/satellite/99722#data with satnogs account. (temporary URL until NORAD coordination)

Check out SatNOGS at: https://satnogs.org/